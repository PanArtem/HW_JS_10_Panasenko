const firstInput = document.getElementById('first-input-pass'),
      secondInput = document.getElementById('second-input-pass');
const firstIconPass = document.getElementById('first-fa-eye'),
      secondIconPass = document.getElementById('second-fa-eye');
const btn = document.getElementById('btn');
const secondContainerPass = document.getElementById('second-container-pass');
const alertSpan = document.createElement('span');
        secondContainerPass.appendChild(alertSpan);
        alertSpan.classList.add('alert-span');

function showPass (input, icon) {
    if (input.type === 'password') {
        input.type = 'text';
        icon.classList.add('fa-eye-slash');
        icon.classList.remove('fa-eye');
    } else {
        input.type = 'password';
        icon.classList.add('fa-eye');
        icon.classList.remove('fa-eye-slash');
    }
}

firstIconPass.onclick = () => {
    showPass(firstInput, firstIconPass)
};

secondIconPass.onclick = () => {
    showPass(secondInput, secondIconPass)
};

btn.addEventListener('click', () => {
    if (firstInput.value === secondInput.value ) {
        alert('You are welcome');
        alertSpan.style.display = "none";
    } else {
        alertSpan.innerHTML = 'Нужно ввести одинаковые значения';
        alertSpan.style.display = "block";
    }
});